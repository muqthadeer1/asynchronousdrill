// Create a function named parallelLimit.
// This function should take an array of Promises and a limit parameter (number of Promises that can run in parallel).
// Execute the Promises in parallel, but ensure that no more than the specified limit are running simultaneously.
// The function should resolve with an array containing the results of all the Promises.

//define the function
function fnProblem4(){

    //define a function with two parameters promises and limit
function parallelLimit(promises,limit){
    return new Promise((resovle,reject)=>{
        let minValue=Math.min(promises.length,limit);
        let array=promises.slice(0,minValue);
        Promise.allSettled(array).then((firstResult)=>{
            let count=limit;
            let secondArray=[];
            function recusrsive(){
                if(count>=promises.length){
                    let finalResult=firstResult.concat(secondArray);
                    resovle(finalResult);
                    // return;
                }
                if(count < promises.length){
                    promises[count].then((innerResult)=>{
                        secondArray.push(innerResult);
                        recusrsive();
                    }).catch(err=>{
                        secondArray.push(err);
                        recusrsive();
                    })
                }
                count+=1;
            }
            recusrsive();
        }).catch(err=>{
            console.log(err);
        })
    })
}


function main(){
    let promise1=Promise.resolve(3);
    let promise2=Promise.reject("fail");
    let promise3=Promise.resolve("success");
    let promise4=new Promise((resolve,reject)=>{
        reject("I dont like it");
    })

    let array=[promise1,promise2,promise3,promise4];
    parallelLimit(array,2).then((result)=>{
        console.log(result);
    }).catch(err=>{
        console.log(err);
    });
}

main();

}

//create  a module to export the function to other file
module.exports = fnProblem4;

