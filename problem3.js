//define the function 
function fnProblem3(){

    //define the first function with promises
function promise1() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("This First promise");
      }, 1000);
    });
  }
  
    //define the second function with promises
  function promise2() {
    return Promise.resolve("Passed first promise and This second promise");
  }
  
    //define the third function with promises
  function promise3() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("passed first and second promises and This is Third Promise.");
      }, 2000);
    });
  }
  
  //create a variable which store three promises in the form of array
  let functionsArray = [promise1, promise2, promise3];
//   console.log(functionsArray.length)


//call function
  function dynamicChain(functionsArray) {
    return new Promise((resolve, reject) => {
      if (!Array.isArray(functionsArray) || functionsArray.length === 0) {
        reject(new Error("Invalid array of functions"));
        return;
      }
  
      // create variable which store the pending promise
      let resultPromise = Promise.resolve();
    //   console.log(resultPromise);
  
      for (let index = 0; index < functionsArray.length; index++) {
        resultPromise = resultPromise.then(functionsArray[index]);

      }
  
      resultPromise
        .then((result) => resolve(result)) 
        .catch((err) => reject(err));
    });
  }
  
  dynamicChain(functionsArray)
    .then((result) => console.log(result))
    .catch((err) => console.error(err));
}

//create a module to export the function to other files
module.exports = fnProblem3;