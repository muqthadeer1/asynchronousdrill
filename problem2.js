// define the function 
function fnProblem2(){

  // define the function with promises as argument 
function composePromises(promises) {
    return Promise.allSettled(promises);
  }
  
  // create a promises
  const promise1 = Promise.resolve("Result from Promise 1");
  const promise2 =new Promise((resolve) => setTimeout(() => resolve("Result from Promise 2"), 1000));
  const promise3 = Promise.reject("Error from Promise 3");
  
  // called a function with promises as arguments
  const fnComposedPromise = composePromises([promise1, promise2, promise3]);

  fnComposedPromise
    .then((results) => {
      console.log("Results from composed promises:", results);
    })
    .catch((error) => {
      console.log("One of the promises rejected:", error);
    });
}

//create a module to export the function to other files
module.exports = fnProblem2
