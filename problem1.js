// define the function 
function fnProblem1(){
// define a function with promises
function racePromise1(){
    return new Promise((resolved,reject)=>{
       setTimeout(() => {
        const success = true;
        if(success){
            resolved("Resolve Successfully racePromise1");
        } else {
            reject("failed to fetch");
        }

       },1000);   
    })
}

// define a function with promises
function racePromise2(){
    return new Promise((resolved,reject)=>{
        setTimeout(() => {
          const success =true;
          if(success){
            resolved("Resolve Successfully racePromise2");
          }  else {
            reject("failed to fetch")
          }
        },3000);
    })
}
// create a variable and store first function
const race1 = racePromise1();
//create a variable and store second function
const race2 = racePromise2();

//using Promise.race method to see which function settled first
Promise.race([race1,race2])
.then((result)=>{
    console.log("first Sellted",result)
})
.catch((error)=>{
    console.log("error:",error);
})

}

// create a module to export the function to other function
module.exports = fnProblem1;